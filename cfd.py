import modelling as m
import numpy as np
import os
import pandas as pd
import subprocess

quiet_args = {"stdout": open(os.devnull, "w"), "stderr": subprocess.STDOUT}


class cfd:
    def __init__(self, vessel_name="model"):
        self.vessel_name = vessel_name

        # Number of decimal places to keep in the cache
        self.precision = m.overall_params.precision

        # Counters
        self.cache_hits = _Counter()
        self.cfd_executions = _Counter()

        self._cache_path = (
            os.path.dirname(os.path.realpath(__file__)) + "/" + vessel_name + ".hdf5"
        )

        model = m.BoxParametricModel()
        try:
            # Load cache from file and check if it is initialised
            self.cache = pd.read_hdf(self._cache_path)
            self.cache[tuple(m.serialise(model))]
        except (FileNotFoundError, KeyError, IndexError):
            # Initialise cache with a dict with a tuple of the right dimensions
            # as a key
            self.cache = pd.Series({tuple([None for _ in m.serialise(model)]): None})
            # Replace the cache, removing the initialisation dict
            self.cache = pd.Series(
                {tuple(m.serialise(model)): cfd.__call__(self, model)}
            )
            # Save the cache to file
            self.cache.to_hdf(self._cache_path, self.vessel_name)

    def __call__(self, model: m.ParametricModel):
        """Cached function that returns resistance in newtons"""
        serialised_model = tuple(m.serialise(model).round(self.precision))
        try:
            # Return resistance from cache if found
            resistance = self.cache[serialised_model]
            self.cache_hits.increment()
            return resistance
        except KeyError:
            freecad_model = m.FreeCADModel(model)
            old_directory = os.getcwd()
            cfd_directory = os.path.dirname(os.path.realpath(__file__))
            os.chdir(cfd_directory)
            subprocess.run(["./Allclean"], **quiet_args)
            freecad_model.exportStl("constant/triSurface/" + "Hull.stl")
            # STL needs to be moved to trisurface
            # Blockmesh to be used for a bounding box,
            # Followed by simpleFoam using the mesh to calculate resistance
            # Pandas used to extract resistance data
            # Resistance stored in variable resistance...
            subprocess.run(["./Allrun"], **quiet_args)
            df = pd.read_csv(
                "postProcessing/resistance/0/forces.dat",
                sep="[()\t\s]+",
                comment="#",
                engine="python",
            )
            df.columns = list(range(20))
            resistance = abs(df[6].median())
            self.cache[serialised_model] = resistance
            # Save the cache to file
            self.cache.to_hdf(self._cache_path, self.vessel_name)
            os.chdir(old_directory)
            self.cfd_executions.increment()
            return resistance


class _Counter:
    def __init__(self):
        self.count = 0

    def reset(self):
        count = self.count
        self.count = 0
        return count

    def increment(self):
        self.count += 1
